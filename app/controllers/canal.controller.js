const db = require("../models");
const Canal = db.canales;
const Op = db.Sequelize.Op;

// Create and Save a new Canal
exports.create = (req, res) => {
  // Validate request
  if (!req.body.nombre) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a Canal
  const canal = {
    nombre: req.body.nombre,
    aplicacion: req.body.aplicacion,
    duracion_cotizacion: req.body.duracion_cotizacion,
    imagen_cotizacion: req.body.duracion_cotizacion,
    estado_canal: req.body.estado_canal
  };

  // Save Canal in the database
  Canal.create(canal)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Canal."
      });
    });
};

// Retrieve all from the database.
exports.findAll = (req, res) => {
  const title = req.query.nombre;
  var condition = title ? { nombre: { [Op.like]: `%${title}%` } } : null;

  Canal.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving canales."
      });
    });
};

// Find a single Canal with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Canal.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Canal with id=" + id
      });
    });
};

// Update a Canal by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  Canal.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Canal was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update with id=${id}. Maybe was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Canal with id=" + id
      });
    });
};

// Delete a Canal with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Canal.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Canal was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete with id=${id}. Maybe was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Canal with id=" + id
      });
    });
};

// Delete all from the database.
exports.deleteAll = (req, res) => {
  Canal.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Tutorials were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all canales."
      });
    });
};

// find all published Canal
exports.findAllPublished = (req, res) => {
  Canal.findAll({ where: { published: true } })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving canales."
      });
    });
};
