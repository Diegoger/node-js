const db = require("../models");
const Aplicacion = db.aplicaciones;
const Op = db.Sequelize.Op;

// Create and Save a new Aplicacion
exports.create = (req, res) => {
  // Validate request
  if (!req.body.nombre) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a Aplicacion
  const aplicacion = {
    nombre: req.body.nombre,
    estado: req.body.estado,
  };

  // Save Aplicacion in the database
  Aplicacion.create(aplicacion)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Aplicacion."
      });
    });
};

// Retrieve all from the database.
exports.findAll = (req, res) => {
  const title = req.query.nombre;
  var condition = title ? { nombre: { [Op.like]: `%${title}%` } } : null;

  Aplicacion.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving aplicaciones."
      });
    });
};

// Find a single Aplicacion with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Aplicacion.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Aplicacion with id=" + id
      });
    });
};

// Update a Aplicacion by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  Aplicacion.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Aplicacion was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update with id=${id}. Maybe was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Aplicacion with id=" + id
      });
    });
};

// Delete a Aplicacion with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Aplicacion.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Aplicacion was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete with id=${id}. Maybe was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Aplicacion with id=" + id
      });
    });
};

// Delete all from the database.
exports.deleteAll = (req, res) => {
  Aplicacion.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Tutorials were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all aplicaciones."
      });
    });
};

// find all published Aplicacion
exports.findAllPublished = (req, res) => {
  Aplicacion.findAll({ where: { published: true } })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving aplicaciones."
      });
    });
};
