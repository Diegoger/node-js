module.exports = (sequelize, Sequelize) => {
  const Canal = sequelize.define("canales", {
    nombre: {
      type: Sequelize.STRING
    },
    aplicacion: {
      type: Sequelize.INTEGER
    },
    duracion_cotizacion: {
      type: Sequelize.FLOAT
    },
    imagen_cotizacion: {
      type: Sequelize.BOOLEAN
    },
    estado_canal: {
      type: Sequelize.BOOLEAN
    }
  });

  return Canal;
};
