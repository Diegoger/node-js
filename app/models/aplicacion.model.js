module.exports = (sequelize, Sequelize) => {
  const Aplicacion = sequelize.define("aplicacion", {
    nombre: {
      type: Sequelize.STRING
    },
    estado: {
      type: Sequelize.BOOLEAN
    }
  });

  return Aplicacion;
};
