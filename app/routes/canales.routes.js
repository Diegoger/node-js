module.exports = app => {
  const canales = require("../controllers/canal.controller.js");

  var router = require("express").Router();

  router.post("/", canales.create);

  router.get("/", canales.findAll);

  router.get("/estado", canales.findAllPublished);

  router.get("/:id", canales.findOne);

  router.put("/:id", canales.update);

  router.delete("/:id", canales.delete);

  router.delete("/", canales.deleteAll);

  app.use('/api/canales', router);
};
