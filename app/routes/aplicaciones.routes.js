module.exports = app => {
  const aplicaciones = require("../controllers/aplicacion.controller.js");

  var router = require("express").Router();

  router.post("/", aplicaciones.create);

  router.get("/", aplicaciones.findAll);

  router.get("/estado", aplicaciones.findAllPublished);

  router.get("/:id", aplicaciones.findOne);

  router.put("/:id", aplicaciones.update);

  router.delete("/:id", aplicaciones.delete);

  router.delete("/", aplicaciones.deleteAll);

  app.use('/api/aplicaciones', router);
};
